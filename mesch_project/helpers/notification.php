<?php

/*
  mesch.ch project management

  Copyright 2011 mesch web consulting & design GmbH,
  all portions of this codebase are copyrighted to the people
  listed in contributors.txt.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

class NotificationHelper {

    public function sendMailNotification($comment) {
        $c = Page::getCurrentPage();

        // reload collection version, otherwise we don't get the correct attribute value
        $cv = CollectionVersion::get($c, 'RECENT');
        $blocks = $c->getBlocks();

        $u = new User();
        $uID = $u->getUserID();

        $recipients = array();

        // add current issue assignee to list of recipients      
        $assigneeID = $c->getAttribute('mesch_project_assignee');
        if ($uID != $assigneeID && $assigneeID != '') {
            $recipients[$assigneeID] = $assigneeID;
        }

        // add user who created page
        if ($uID != $c->getCollectionUserID()) {
            $recipients[$c->getCollectionUserID()] = $c->getCollectionUserID();
        }

        // add all users who posted a comment
        foreach ($blocks as $block) {
            if ($block->getBlockTypeHandle() == 'mesch_project_comment') {
                $blockInstance = $block->getInstance();

                // ignore current user
                if ($uID == $blockInstance->uID)
                    continue;

                $recipients[$blockInstance->uID] = $blockInstance->uID;
            }
        }

        $nh = Loader::helper('navigation');

        foreach ($recipients as $recipientID) {
            $ui = UserInfo::getByID($recipientID);

            if (is_object($ui)) {
                $mh = Loader::helper('mail');
                $mh->addParameter('subject', $c->getCollectionName());
                $mh->addParameter('text', $comment);
                $mh->addParameter('recipient', $ui->getUserName());
                $mh->addParameter('team', SITE);
                $mh->addParameter('link', $nh->getLinkToCollection($c, true));
                $mh->load('message_notification', 'mesch_project');
                $mh->to($ui->getUserEmail());
                $mh->sendMail();
            }
        }
    }

}

?>