<?php
/*
  mesch.ch project management

  Copyright 2011 mesch web consulting & design GmbH,
  all portions of this codebase are copyrighted to the people
  listed in contributors.txt.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

defined('C5_EXECUTE') or die(_("Access Denied."));
?>
<!DOCTYPE html>
<html lang="de_DE">
    <head>

        <!-- created by mesch web consulting & design GmbH <?php echo date('Y') ?> --> 
        <!-- consulting - webdesign - programmierungen - cms - support --> 
        <!-- www.mesch.ch --> 

        <meta charset="utf-8" />

        <script type="text/javascript">
            <?php
            Loader::model('project_list', 'mesch_project');
            $pl = new ProjectList();
            ?>
            var projects = [<?php echo $pl->getList(true); ?>];
        </script>

        <?php
        Loader::element('header_required');
        
        $perm = new Permissions($c);
        $canWrite = $perm->canWrite();
        ?>

        
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath() ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath() ?>/main.css" />
        <link rel="stylesheet" media="print" type="text/css" href="<?php echo $this->getThemePath() ?>/print.css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath() ?>/css/jquery.autocomplete.css" />
        <script src="<?php echo $this->getThemePath() ?>/js/modernizr-1.7.min.js"></script>
        <script src="<?php echo $this->getThemePath() ?>/js/jquery.autocomplete.min.js"></script>
        <script src="<?php echo $this->getThemePath() ?>/js/jquery.tablesorter.js"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                var mytemp = '';
                var tables = $('table').length;
                $('table').each(function (idx) {
                    if (idx + 1 <= tables) {
                        var tds = $('table tr th').length;
                        $('table td').each(function (i, val, id) {
                            if (isValidDate($(this).text())) {
                                if (i <= tds) {
                                    mytemp += i + ',';
                                    $(this).addClass(mytemp);
                                }
                            }
                        });
                    }
                })
                $("table").tablesorter({
                    debug: true,
                    dateFormat: 'dd.mm.yyyy',
                    headers: {
                        mytemp: {sorter: 'shortDate'},
                    }});


                $("table th").css('cursor', 'pointer');
                $("#navigation-projects-select").result(function (event, data, formatted) {
                    window.location.href = CCM_DISPATCHER_FILENAME + "?cID=" + data.cID;
                });

                $("#navigation-phone-text").keypress(function (e) {
                    code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        e.preventDefault();

                        $.post(CCM_TOOLS_PATH + "/../packages/mesch_project/phone_lookup/", {query: $("#navigation-phone-text").val()},
                        function (data) {
                            alert(data);
                        });

                    }

                });

                $("#navigation-projects-select").autocomplete(projects, {
                    autoFill: false,
                    max: 500,
                    matchContains: true,
                    minChars: 0,
                    formatItem: function (row, i, max) {
                        return row.name;
                    },
                    formatMatch: function (row, i, max) {
                        return row.name;
                    },
                    formatResult: function (row) {
                        return row.name;
                    }
                });
                // Expect input as d/m/y
                function isValidDate (s) {
                    var bits = s.split('.');
                    var d = new Date(bits[2] + '/' + bits[1] + '/' + bits[0]);
                    return !!(d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]));
                }
            });
        </script>

    </head>

    <body>

        <div id="wrapper"<?php echo $canWrite ? 'style="margin-top: 49px;"': ''?>>

            <nav id="navigation" class="navbar navbar-inverse navbar-fixed-top"<?php echo $canWrite ? 'style="margin-top: 49px;"': ''?>>
                <div class="navbar-inner">
                    <ul class="nav">
                        <li><a href="/">Home</a></li>

                        <?php
                        $nh = Loader::helper('navigation');

                        $homePage = Page::getByID(1);
                        foreach ($homePage->getCollectionChildrenArray(1) as $childPageId) {
                            $child = Page::getByID($childPageId);

                            if ($child->isSystemPage())
                                continue;

                            $p = new Permissions($child);
                            if (!$p->canRead())
                                continue;


                            echo "<li><a href=\"{$nh->getLinkToCollection($child)}\">{$child->getCollectionName()}</a></li>";
                        }
                        ?>       
                    </ul>

                    <div id="navigation-projects" class="navbar-form pull-right">            
                        <?php echo t('Projects:') ?>
                        <input type="text" name="navigation-projects-select" id="navigation-projects-select"/>
                    </div>

                </div>
            </nav>

